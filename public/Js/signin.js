function initApp() {
    // Login with Email/Password
    var Email  = document.getElementById('Email');
    var Password = document.getElementById('Password');
    var Signin = document.getElementById('Signin');
    var Google = document.getElementById('Google');
    var NewAccount = document.getElementById('NewAccount');

    Signin.addEventListener('click', function () {

        firebase.auth().signInWithEmailAndPassword(Email.value, Password.value)
        .then ( (e) => window.location.assign("lobby.html"))
        .catch( (e) => alert(e.message)); 
        Email.value = "";
        Password.value = "";   
    });

    Google.addEventListener('click', function () {
        
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
        .then ( (e) => window.location.assign("lobby.html"))
        .catch( (e) => alert(e.message)); 
        Email.value = "";
        Password.value = "";   
    });

    NewAccount.addEventListener('click', function () {        
        
        firebase.auth().createUserWithEmailAndPassword(Email.value, Password.value)
        .then ( (e) => alert('Account success , please sign again'))
        .catch( (e) => alert(e.message));
        Email.value = "";
        Password.value = ""; 
    });
}

window.onload = function () {
    initApp();
};
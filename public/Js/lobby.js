window.onload = function () {

    firebase.auth().onAuthStateChanged(function (user) {
        // Check user login
        if (user) {

            var logout = document.getElementById('logout');
            logout.addEventListener('click', () => {
                firebase.auth().signOut()
                .then ((e) => window.location.assign("index.html"));
            });
        }
    });

    var tainan = document.getElementById('tainan');
    tainan.addEventListener('click', () => {
        window.location.assign("exchange.html");
    });

    var taichung = document.getElementById('taichung');
    taichung.addEventListener('click', () => {
        window.location.assign("exchange_taichung.html");
    });

};



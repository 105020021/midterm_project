function init() {

    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        // Check user login
        if (user) {
            user_email = user.email;
            var logout = document.getElementById('logout');
            logout.addEventListener('click', () => {
                firebase.auth().signOut()
                .then ((e) => window.location.assign("index.html"));
            });
        }
    });


    var account = document.getElementById('account');
    account.addEventListener('click', () => {
        window.location.assign("account.html");
    });

    var post_exist = false;
    var add = document.getElementById('add');
    var post = document.getElementById('post');

    post_name = document.getElementById('name');
    post_cp = document.getElementById('cp');
    post_pokemon = document.getElementById('pokemon');
    post_place = document.getElementById('place');

    var cancel = document.getElementById('cancel');
    cancel.addEventListener('click', () => {
        post_name.value = "";
        post_pokemon.value = "";
        post_cp.value = "";
        post_place.value = "";
        post_exist = false;
        post.style.display = "none";
        add.innerHTML = "Add";
    });

    // color
    var color = '';
    var red = document.getElementById('red');
    var blue = document.getElementById('blue');
    var yellow = document.getElementById('yellow');
    var other = document.getElementById('other');

    var colorbox = document.getElementById('colorbox');
    var colorbtn = document.getElementById('colorbtn');
    colorbtn.addEventListener('click', () => {

        colorbox.style.display = "block";

        red.addEventListener('click', () => {
            color = 'red';
            colorbox.style.display = "none";
            colorbtn.style.backgroundColor = "red";
        });
        blue.addEventListener('click', () => {
            color = 'blue';
            colorbox.style.display = "none";
            colorbtn.style.backgroundColor = "blue";
        });
        yellow.addEventListener('click', () => {
            color = 'yellow';
            colorbox.style.display = "none";
            colorbtn.style.backgroundColor = "yellow";
        });
        other.addEventListener('click', () => {
            color = 'none';
            colorbox.style.display = "none";
            colorbtn.style.backgroundColor = "dimgray";
        });

    });

    add.addEventListener('click', () => {

        if(post_exist === false){
            post_exist = true;
            post.style.display = "block";
            post.style.display = "flex";
            post.style.animationName = "show";
            post.style.animationDuration = "1s";
            post.style.animationIterationCount = "1";
            add.innerHTML = "Enter";
        }
        else{

            // white data
            if (post_name.value != "" && post_pokemon.value != "" && post_cp.value != "" && post_place.value != "") {

                var loginUser = firebase.auth().currentUser;
                var userRef = firebase.database().ref('Users/' + loginUser.uid).push();
                userRef.set({
                    email: user_email,
                    name: post_name.value,
                    pokemon: post_pokemon.value,
                    cp: post_cp.value,
                    place: post_place.value
                });

                if(color === 'red'){
                    var colorRef = firebase.database().ref('Fire_taichung/').push();
                    colorRef.set({
                        email: user_email,
                        name: post_name.value,
                        pokemon: post_pokemon.value,
                        cp: post_cp.value,
                        place: post_place.value
                    });
                }
                if(color === 'blue'){
                    var colorRef = firebase.database().ref('Water_taichung/').push();
                    colorRef.set({
                        email: user_email,
                        name: post_name.value,
                        pokemon: post_pokemon.value,
                        cp: post_cp.value,
                        place: post_place.value
                    });
                }
                if(color === 'yellow'){
                    var colorRef = firebase.database().ref('Electric_taichung/').push();
                    colorRef.set({
                        email: user_email,
                        name: post_name.value,
                        pokemon: post_pokemon.value,
                        cp: post_cp.value,
                        place: post_place.value
                    });
                }

                var newpostref = firebase.database().ref('card_list_taichung').push();
                newpostref.set({
                    email: user_email,
                    name: post_name.value,
                    pokemon: post_pokemon.value,
                    cp: post_cp.value,
                    place: post_place.value
                });
                post_name.value = "";
                post_pokemon.value = "";
                post_cp.value = "";
                post_place.value = "";
                // close the post
                post_exist = false;
                post.style.display = "none";
                add.innerHTML = "Add";
            }
            else {
                alert('There is information need to be added');
            }
        }
    });

    var postsRef = firebase.database().ref('card_list_taichung/');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    var all_post_list = document.getElementById('all_post_list');

    postsRef.once('value').then(function (snapshot) {
            
        snapshot.forEach(function (childSnapshot){
            var childData = childSnapshot.val();
            var i = childSnapshot.key;
            total_post[total_post.length] = "<div class='postbox'>"
                                            + "<div class='boxinfo'> User:   " + childData.email + "</div>"
                                            + "<div class='boxinfo'> Player's ID:   " + childData.name + "</div>"
                                            + "<div class='boxinfo'> Pokemon:   " + childData.pokemon + "</div>"
                                            + "<div class='boxinfo'> Cp:   " + childData.cp + "</div>"
                                            + "<div class='boxinfo'> Ps:   " + childData.place + "</div>"
                                            + "<a href = 'comment_taichung.html?"+ i +"','>" + 'Reply' + '</a>' 
                                            + "</div>\n";
            first_count += 1;
        })
        all_post_list.innerHTML = total_post.join('');
        postsRef.on('child_added',function (data){
            second_count += 1;
            if(second_count > first_count){
                var childData = data.val();
                var j = data.key;
                total_post[total_post.length] = "<div class='postbox'>"
                                                + "<div class='boxinfo'> User:   " + childData.email + "</div>"
                                                + "<div class='boxinfo'> Player's ID:   " + childData.name + "</div>"
                                                + "<div class='boxinfo'> Pokemon:   " + childData.pokemon + "</div>"
                                                + "<div class='boxinfo'> Cp:   " + childData.cp + "</div>"
                                                + "<div class='boxinfo'> Ps:   " + childData.place + "</div>"
                                                + "<a href = 'comment_taichung.html?"+ j +"','>" + 'Reply' + '</a>' 
                                                + "</div>\n";
                all_post_list.innerHTML = total_post.join('');
            }
        });
    })

    // fire
    var fire_postsRef = firebase.database().ref('Fire_taichung/');
    fire_total_post = [];
    first_count = 0;
    second_count = 0;
    var fire_post_list = document.getElementById('fire_post_list');

    fire_postsRef.once('value').then(function (snapshot) {
            
        snapshot.forEach(function (childSnapshot){
            var childData = childSnapshot.val();
            var i = childSnapshot.key;
            fire_total_post[fire_total_post.length] = "<div class='postbox'>"
                                                    + "<div class='boxinfo'> User:   " + childData.email + "</div>"
                                                    + "<div class='boxinfo'> Player's ID:   " + childData.name + "</div>"
                                                    + "<div class='boxinfo'> Pokemon:   " + childData.pokemon + "</div>"
                                                    + "<div class='boxinfo'> Cp:   " + childData.cp + "</div>"
                                                    + "<div class='boxinfo'> Ps:   " + childData.place + "</div>"
                                                    + "<a href = 'comment_taichung.html?"+ i +"','>" + 'Reply' + '</a>' 
                                                    + "</div>\n";
            first_count += 1;
        })
        fire_post_list.innerHTML = fire_total_post.join('');
        fire_postsRef.on('child_added',function (data){
            second_count += 1;
            if(second_count > first_count){
                var childData = data.val();
                var j = data.key;
                fire_total_post[fire_total_post.length] = "<div class='postbox'>"
                                                        + "<div class='boxinfo'> User:   " + childData.email + "</div>"
                                                        + "<div class='boxinfo'> Player's ID:   " + childData.name + "</div>"
                                                        + "<div class='boxinfo'> Pokemon:   " + childData.pokemon + "</div>"
                                                        + "<div class='boxinfo'> Cp:   " + childData.cp + "</div>"
                                                        + "<div class='boxinfo'> Ps:   " + childData.place + "</div>"
                                                        + "<a href = 'comment_taichung.html?"+ j +"','>" + 'Reply' + '</a>' 
                                                        + "</div>\n";
                fire_post_list.innerHTML = fire_total_post.join('');
            }
        });
    })

    // water
    var water_postsRef = firebase.database().ref('Water_taichung/');
    water_total_post = [];
    first_count = 0;
    second_count = 0;
    var water_post_list = document.getElementById('water_post_list');

    water_postsRef.once('value').then(function (snapshot) {
            
        snapshot.forEach(function (childSnapshot){
            var childData = childSnapshot.val();
            var i = childSnapshot.key;
            water_total_post[water_total_post.length] = "<div class='postbox'>"
                                            + "<div class='boxinfo'> User:   " + childData.email + "</div>"
                                            + "<div class='boxinfo'> Player's ID:   " + childData.name + "</div>"
                                            + "<div class='boxinfo'> Pokemon:   " + childData.pokemon + "</div>"
                                            + "<div class='boxinfo'> Cp:   " + childData.cp + "</div>"
                                            + "<div class='boxinfo'> Ps:   " + childData.place + "</div>"
                                            + "<a href = 'comment_taichung.html?"+ i +"','>" + 'Reply' + '</a>' 
                                            + "</div>\n";
            first_count += 1;
        })
        water_post_list.innerHTML = water_total_post.join('');
        water_postsRef.on('child_added',function (data){
            second_count += 1;
            if(second_count > first_count){
                var childData = data.val();
                var j = data.key;
                water_total_post[water_total_post.length] = "<div class='postbox'>"
                                                + "<div class='boxinfo'> User:   " + childData.email + "</div>"
                                                + "<div class='boxinfo'> Player's ID:   " + childData.name + "</div>"
                                                + "<div class='boxinfo'> Pokemon:   " + childData.pokemon + "</div>"
                                                + "<div class='boxinfo'> Cp:   " + childData.cp + "</div>"
                                                + "<div class='boxinfo'> Ps:   " + childData.place + "</div>"
                                                + "<a href = 'comment_taichung.html?"+ j +"','>" + 'Reply' + '</a>' 
                                                + "</div>\n";
                water_post_list.innerHTML = water_total_post.join('');
            }
        });
    })

    // electric
    var electric_postsRef = firebase.database().ref('Electric_taichung/');
    electric_total_post = [];
    first_count = 0;
    second_count = 0;
    var electric_post_list = document.getElementById('electric_post_list');

    electric_postsRef.once('value').then(function (snapshot) {
            
        snapshot.forEach(function (childSnapshot){
            var childData = childSnapshot.val();
            var i = childSnapshot.key;
            electric_total_post[electric_total_post.length] = "<div class='postbox'>"
                                            + "<div class='boxinfo'> User:   " + childData.email + "</div>"
                                            + "<div class='boxinfo'> Player's ID:   " + childData.name + "</div>"
                                            + "<div class='boxinfo'> Pokemon:   " + childData.pokemon + "</div>"
                                            + "<div class='boxinfo'> Cp:   " + childData.cp + "</div>"
                                            + "<div class='boxinfo'> Ps:   " + childData.place + "</div>"
                                            + "<a href = 'comment_taichung.html?"+ i +"','>" + 'Reply' + '</a>' 
                                            + "</div>\n";
            first_count += 1;
        })
        electric_post_list.innerHTML = electric_total_post.join('');
        electric_postsRef.on('child_added',function (data){
            second_count += 1;
            if(second_count > first_count){
                var childData = data.val();
                var j = data.key;
                electric_total_post[electric_total_post.length] = "<div class='postbox'>"
                                                + "<div class='boxinfo'> User:   " + childData.email + "</div>"
                                                + "<div class='boxinfo'> Player's ID:   " + childData.name + "</div>"
                                                + "<div class='boxinfo'> Pokemon:   " + childData.pokemon + "</div>"
                                                + "<div class='boxinfo'> Cp:   " + childData.cp + "</div>"
                                                + "<div class='boxinfo'> Ps:   " + childData.place + "</div>"
                                                + "<a href = 'comment_taichung.html?"+ j +"','>" + 'Reply' + '</a>' 
                                                + "</div>\n";
                electric_post_list.innerHTML = electric_total_post.join('');
            }
        });
    })

    all_post_list.style.display = "block"

    var fire = document.getElementById('fire');
    fire.addEventListener('click', () => {
        all_post_list.style.display = "none";
        fire_post_list.style.display = "block";
        water_post_list.style.display = "none";
        electric_post_list.style.display = "none";
    })
    var water = document.getElementById('water');
    water.addEventListener('click', () => {
        all_post_list.style.display = "none";
        fire_post_list.style.display = "none";
        water_post_list.style.display = "block";
        electric_post_list.style.display = "none";
    })
    var electric = document.getElementById('electric');
    electric.addEventListener('click', () => {
        all_post_list.style.display = "none";
        fire_post_list.style.display = "none";
        water_post_list.style.display = "none";
        electric_post_list.style.display = "block";
    })
    var all = document.getElementById('all');
    all.addEventListener('click', () => {
        all_post_list.style.display = "block";
        fire_post_list.style.display = "none";
        water_post_list.style.display = "none";
        electric_post_list.style.display = "none";
    })
    
}

window.onload = function () {
    init();
};
function init() {

    var account_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        // Check user login
        if (user) {
            account_email = user.email;
            var logout = document.getElementById('logout');
            logout.addEventListener('click', () => {
                firebase.auth().signOut()
                .then ((e) => window.location.assign("index.html"));
            });
        }

        Password = document.getElementById("account");
        Password.addEventListener('click', function() {
            var auth = firebase.auth();
            var emailAddress = account_email;
            auth.sendPasswordResetEmail(emailAddress).then(function() {
                alert("Please checkout your email!");
            });
        });

        var postsRef = firebase.database().ref('Users/' + user.uid);

        var total_post = [];
        var first_count = 0;
        var second_count = 0;
        var post_list = document.getElementById('history_list');

        postsRef.once('value').then(function (snapshot) {
                
            snapshot.forEach(function (childSnapshot){
                var childData = childSnapshot.val();
                total_post[total_post.length] = "<div class='historybox'>"
                                                + "<div class='historyinfo'> User:   " + childData.email + "</div>"
                                                + "<div class='historyinfo'> Player's ID:   " + childData.name + "</div>"
                                                + "<div class='historyinfo'> Pokemon:   " + childData.pokemon + "</div>"
                                                + "<div class='historyinfo'> Cp:   " + childData.cp + "</div>"
                                                + "<div class='historyinfo'> Ps:   " + childData.place + "</div>"  
                                                + "</div>\n";
                first_count += 1;
            })
            post_list.innerHTML = total_post.join('');
            postsRef.on('child_added',function (data){
                second_count += 1;
                if(second_count > first_count){
                    var childData = data.val();
                    total_post[total_post.length] = "<div class='historybox'>"
                                                    + "<div class='historyinfo'> User:   " + childData.email + "</div>"
                                                    + "<div class='historyinfo'> Player's ID:   " + childData.name + "</div>"
                                                    + "<div class='historyinfo'> Pokemon:   " + childData.pokemon + "</div>"
                                                    + "<div class='historyinfo'> Cp:   " + childData.cp + "</div>"
                                                    + "<div class='historyinfo'> Ps:   " + childData.place + "</div>" 
                                                    + "</div>\n";
                    post_list.innerHTML = total_post.join('');
                }
            });
        });
    });
}

window.onload = function () {
    init();
};

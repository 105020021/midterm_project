function init() {

    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        // Check user login
        if (user) {
            user_email = user.email;
            var logout = document.getElementById('logout');
            logout.addEventListener('click', () => {
                firebase.auth().signOut()
                .then ((e) => window.location.assign("index.html"));
            });
        }
    });

    var href = location.href;
    var i = href.substring(href.indexOf("?") + 1);

    reply = document.getElementById('reply');
    reply_txt = document.getElementById('content');

    reply.addEventListener('click', () => {

        if (reply_txt.value != "") {

            var newpostref = firebase.database().ref('card_list_taichung/' + i + '/comment').push();
            newpostref.set({
                txt : reply_txt.value,
                user: user_email
            });
            reply_txt.value = '';   
        }
        else {
            alert('There is information need to be added');
        }
    });

    var postRef = firebase.database().ref('card_list_taichung/' + i);
    var post = document.getElementById('authorbox');

    postRef.once('value').then(function (snapshot) {
        
        var childData = snapshot.val();
        post.innerHTML = "<div class='authorbox'>"
                        + "<div class='authorinfo'> User:   " + childData.email + "</div>"
                        + "<div class='authorinfo'> Player's ID:   " + childData.name + "</div>"
                        + "<div class='authorinfo'> Pokemon:   " + childData.pokemon + "</div>"
                        + "<div class='authorinfo'> Cp:   " + childData.cp + "</div>"
                        + "<div class='authorinfo'> Ps:   " + childData.place + "</div>"
                        + "</div>\n";
    })
    .then(function () {

        var postsRef = firebase.database().ref('card_list_taichung/' + i + '/comment');
        var amp = [];
        var total_post = [];
        var temp = 0;
        var first_count = 0;
        var second_count = 0;
        
        var reply_list = document.getElementById('reply_list');

        postsRef.once('value').then(function (snapshot) {
                
            snapshot.forEach(function(childSnapshot){
                
                var childData = childSnapshot.val();
                var temp2 = temp.toString();
                total_post[total_post.length] = "<div class='replybox'>" 
                                                + "<div class='replyinfo'> Author:  " + childData.user + "</div>"
                                                + "<div class='replyinfo'> Comment: " + "<span id=" + temp2 + "></span></div>"  
                                                + "</div>\n";
                first_count += 1;
                amp[amp.length] = childData.txt;
                temp+=1;
            });
            reply_list.innerHTML = total_post.join('');
            
            for(a = 0; a < temp; a++){
                var temp2 = a.toString();
                document.getElementById(temp2).innerText = amp[a];
            }
            
            postsRef.on('child_added',function(data){
                second_count += 1;
                if(second_count > first_count){
                    var temp2 = temp.toString();
                    var childData = data.val();
                    total_post[total_post.length] = "<div class='replybox'>" 
                                                    + "<div class='replyinfo'> Author:  " + childData.user + "</div>"
                                                    + "<div class='replyinfo'> Comment: " + "<span id=" + temp2 + "></span></div>"  
                                                    + "</div>\n";
                    amp[amp.length] = childData.txt;
                    temp += 1;
                    reply_list.innerHTML = total_post.join('');
                    
                    for(a = 0; a < temp; a++){
                        var temp2 = a.toString();
                        document.getElementById(temp2).innerText = amp[a];
                    }
                }
            });
        })
    })
}

window.onload = function () {
    init();
};

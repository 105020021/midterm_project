# Software Studio 2019 Spring Midterm Project

# 作品網址：[https://pokemon-forum.firebaseapp.com]

## Topic
* Project Name : [Pokemon Forum]
* Key functions 
    1.signin page: the first page, can signin, signup or signin with google account
    1.lobby: can choose different forum ( tainan , taichung ) 
    2.post page: can add the new post or watch other posts
    3.reply page: can reply the specific post
    4.user page: show the user's history post 
    5.Css animation is used in signin page to change the background pictures
    6.Can work on smartphones 
    
* Other functions 
    1. reset password: in user page you can press reset button and you will get an email to reset password
    2. classify by pokemon attribute: in post page you can choose attribute to show the specific group of pokemon
    3. user page can show the user's history posts

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description
登入網頁後，選擇想要進入的專版(台南或台中)，進入後可看到該版的文章
如果要新加入文章點左上角的ADD填完資料後再點左上角同一地方的ENTER
要回復某篇文章則點選該文章右方的reply
進入reply頁面後點即可看到大家留言也可以自己留言
點選account即可進入帳戶頁面，可以修改密碼即看到自己以前的紀錄

## Security Report (Optional)
留言時，為避免被動到原本網頁，因此將存放方式從innerHTML改成innerText。
留言及觀看文章必須是登入中的用戶，且不同地區專版看不到其他地區內容。
